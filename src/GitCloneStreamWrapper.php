<?php

namespace Drupal\git_clone;

use DrupalLocalStreamWrapper;

/**
 * Drupal system stream wrapper abstract class.
 */
class GitCloneStreamWrapper extends DrupalLocalStreamWrapper {

  /**
   * List of already determined local path URIs.
   *
   * @var array
   */
  protected static $localPaths = array();

  /**
   * Flag indicating whether in development mode.
   *
   * @var bool
   */
  protected $devMode = FALSE;

  /**
   * An array of development paths to use instead.
   *
   * @var bool
   */
  protected $devPaths = array();

  /**
   * {@inheritdoc}
   */
  public function __construct() {
    $this->devMode = \variable_get('git_clone_dev', FALSE);
    $this->devPaths = \variable_get('git_clone_dev_paths', array());
  }

  /**
   * Determines the file system path used for storing cloned git repositories.
   *
   * @param bool $absolute
   *   Flag determining whether to return an absolute system path.
   * @param bool $prepare
   *   Flag determining whether to attempt creating the path if it
   *   does not exist and modify permissions if it isn't writable.
   *
   * @return string|false
   *   The file system path or FALSE if not set.
   */
  public static function directoryPath($absolute = FALSE, $prepare = FALSE) {
    static $path;

    if (isset($path)) {
      return $path;
    }

    $path = \variable_get('file_git_clone_path', FALSE);
    if (!$path) {
      $path = \drupal_realpath('public://') . '/git_clone';
    }

    if ($path && $prepare && \file_prepare_directory($path, FILE_CREATE_DIRECTORY | FILE_MODIFY_PERMISSIONS)) {
      \file_create_htaccess($path);
    }

    if ($path && $absolute) {
      $path = \drupal_realpath($path);
    }

    if (!$path) {
      return FALSE;
    }

    return $path;
  }

  /**
   * {@inheritdoc}
   */
  public function getDirectoryPath() {
    return static::directoryPath();
  }

  /**
   * {@inheritdoc}
   */
  protected function getLocalPath($uri = NULL) {
    if (!isset($uri)) {
      $uri = $this->uri;
    }

    $target  = $this->getTarget($uri);
    $parts   = \array_filter(\explode(DIRECTORY_SEPARATOR, $target));
    $refType = \array_shift($parts);
    $name    = \array_shift($parts);
    $id      = "$refType:$name";

    // Immediately return if already processed.
    if (isset(static::$localPaths[$id])) {
      return static::$localPaths[$id];
    }

    if ($this->devMode && isset($this->devPaths[$refType][$name])) {
      $path = $this->devPaths[$refType][$name];
      $dir = \dirname($path);
    }
    else {
      $dir = $this->getDirectoryPath();
      $path = $dir;
      if ($refType) {
        $path .= "/$refType";
        if ($name) {
          $path .= "/$name";
        }
      }
    }

    // Add support for symlinks.
    if (\is_link($path)) {
      $path = \readlink($path);
      $dir = \dirname($path);
    }

    // This file does not yet exist.
    if (!($realpath = \realpath($path))) {
      $realpath = \realpath(\dirname($path)) . '/' . \drupal_basename($path);
    }

    $localPath = FALSE;
    if ($realpath && ($directory = \realpath($dir)) && \strpos($realpath, $directory) === 0) {
      $localPath = \rtrim("$realpath/" . \implode('/', $parts), '/');
    }

    return static::$localPaths[$id] = $localPath;
  }

  /**
   * {@inheritdoc}
   */
  public function getExternalUrl() {
    return FALSE;
  }

}
