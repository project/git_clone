<?php
/**
 * @file
 * Contains Drupal\git_clone\EntityController.
 */

namespace Drupal\git_clone;

/**
 * Class Entity.
 *
 * @package Drupal\git_clone
 */
class EntityController extends \EntityAPIControllerExportable {

  /**
   * {@inheritdoc}
   */
  public function export($entity, $prefix = '') {
    $vars = get_object_vars($entity);
    unset(
      $vars[$this->statusKey],
      $vars[$this->moduleKey],
      $vars['initialized'],
      $vars['is_new'],
      $vars['output'],
      $vars['rdf_mapping'],
      $vars['refs'],
      $vars['repository'],
      $vars['settings']['default']
    );
    if ($this->nameKey != $this->idKey) {
      unset($vars[$this->idKey]);
    }
    return entity_var_json_export($vars, $prefix);
  }

}
