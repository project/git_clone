<?php

/**
 * @file
 * Git Clone module file.
 */

use Drupal\git_clone\AdminUIController;
use Drupal\git_clone\GitClone;
use Drupal\git_clone\GitCloneStreamWrapper;

/**
 * Implements hook_boot().
 */
function git_clone_boot()
{
  if (!class_exists('\Drupal\git_clone\GitClone')) {
    throw new \RuntimeException('The Git Clone module requires an autoloader. See https://www.drupal.org/project/composer_autoloader or https://www.drupal.org/project/registry_autoload');
  }
}

/**
 * Implements hook_cron().
 */
function git_clone_cron() {
  // Immediately return if in development mode.
  if (variable_get('git_clone_dev', FALSE)) {
    return;
  }


  /** @var \Drupal\git_clone\GitClone[] $clones */
  $clones = entity_load('git_clone');
  foreach ($clones as $clone) {
    // Only queue branches.
    if ($clone->refType === 'branch') {
      $clone->queue();
    }
  }
}

/**
 * Implements hook_cron_queue_info().
 */
function git_clone_cron_queue_info() {
  $queues['git_clone'] = array(
    'worker callback' => '_git_clone_dequeue_callback',
    // Default timeout to 5 minutes.
    // @todo document/make this a UI configurable option.
    'time'            => variable_get('git_clone_timeout', 300),
  );
  return $queues;
}

/**
 * Implements hook_entity_info().
 */
function git_clone_entity_info() {
  $info['git_clone'] = array(
    'label'            => t('Git Clone'),
    'plural label'     => t('Git Clones'),
    'module'           => 'git_clone',
    // Entity.
    'access callback'  => '_git_clone_access_callback',
    'base table'       => 'git_clone',
    'controller class' => '\Drupal\git_clone\EntityController',
    'entity class'     => '\Drupal\git_clone\GitClone',
    'entity keys'      => array(
      'id'    => 'id',
      'label' => 'title',
      'name'  => 'name',
    ),
    'exportable'       => TRUE,
    // Admin.
    'admin ui'         => array(
      'controller class' => '\Drupal\git_clone\AdminUIController',
      'file'             => 'git_clone.admin.inc',
      'path'             => AdminUIController::PATH,
    ),
  );
  return $info;
}

/**
 * Implements hook_ENTITY_TYPE_OPERATION().
 */
function git_clone_git_clone_delete(GitClone $clone) {
  if ($path = $clone->getPath(FALSE, FALSE)) {
    file_unmanaged_delete_recursive($path);
  }
}

/**
 * Implements hook_ENTITY_TYPE_OPERATION().
 */
function git_clone_git_clone_insert(GitClone $clone) {
  $clone->getPath();
}

/**
 * Implements hook_ENTITY_TYPE_OPERATION().
 */
function git_clone_git_clone_update(GitClone $clone) {
  $clone->getPath();
}


/**
 * Implements hook_module_implements_alter().
 */
function git_clone_module_implements_alter(&$implementations, $hook) {
  // Ensure that Git Clone is executed before everything else, especially
  // modules like the the API module. This helps to ensure that all the git
  // clone repository files are updated before other modules consume them.
  if ($hook === 'cron' || $hook === 'cron_queue_info') {
    $module = 'git_clone';
    if (isset($implementations[$module])) {
      $group = array($module => $implementations[$module]);
      unset($implementations[$module]);
      $implementations = $group + $implementations;
    }
  }
}

/**
 * Implements hook_permission().
 */
function git_clone_permission() {
  return array(
    'administer git clones' => array(
      'title'           => t('Administer git clones'),
      'restrict access' => TRUE,
    ),
    'create git clones'     => array(
      'title' => t('Create git clones'),
    ),
    'delete git clones'     => array(
      'title' => t('Delete git clones'),
    ),
    'update git clones'     => array(
      'title' => t('Update git clones'),
    ),
    'view git clones'       => array(
      'title' => t('View git clones'),
    ),
  );
}

/**
 * Implements hook_stream_wrappers().
 */
function git_clone_stream_wrappers() {
  $wrappers['gitclone'] = array(
    'name'        => t('Git clone files'),
    'class'       => '\Drupal\git_clone\GitCloneStreamWrapper',
    'description' => t('Local cloned git repository files.'),
    'type'        => STREAM_WRAPPERS_LOCAL_HIDDEN,
  );
  return $wrappers;
}

/**
 * Implements callback_entity_access().
 *
 * @see entity_access()
 *
 * @internal
 */
function _git_clone_access_callback($op, $entity, $account, $entity_type) {
  global $user;

  if (!isset($account)) {
    $account = $user;
  }

  // Check that the git clone path is set.
  if (!GitCloneStreamWrapper::directoryPath(TRUE, TRUE)) {
    drupal_set_message(t('You must set a writable <a href="!url">file system git clone path</a> before being able to use the Git Clone system.', array(
      '!url' => url('admin/config/media/file-system'),
    )), 'error', FALSE);

    // If there is no git clone path, only let the "view" operation through.
    if ($op !== 'view') {
      return FALSE;
    }
  }

  // User 1 and roles with the "administer git clones" have all privileges.
  if ($account->uid == 1 || user_access('administer git clones')) {
    return TRUE;
  }

  // Check roles that have the following operation permissions.
  $ops = array('create', 'delete', 'update', 'view');
  if (in_array($op, $ops)) {
    return user_access($op . ' git clones', $account);
  }
}

/**
 * Worker callback for git_clone_cron_queue_info().
 *
 * @param \Drupal\git_clone\GitClone $clone
 *   The git clone entity.
 *
 * @see git_clone_cron_queue_info()
 *
 * @internal
 */
function _git_clone_dequeue_callback($clone) {
  if (!($clone instanceof GitClone)) {
    return;
  }

  module_invoke_all('git_clone_pre_dequeue', $clone);
  $clone->dequeue();
  module_invoke_all('git_clone_post_dequeue', $clone);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function git_clone_form_api_branch_edit_form_alter(&$form, &$form_state, $form_id) {
  /** @var GitClone $clone */
  $options = array();
  foreach (entity_load('git_clone') as $clone) {
    $options[$clone->getPath(FALSE, FALSE)] = $clone->label();
  }
  $form['data']['directories']['#title'] = t('Git Clone Repository');
  $form['data']['directories']['#type'] = 'select';
  $form['data']['directories']['#options'] = $options;
  $form['data']['directories']['#description'] = t('Choose the !link that will be associated and used to parse this "API branch".', array(
    '!link' => entity_access('view', 'git_clone') ? l(t('Git Clone Repository'), AdminUIController::PATH) : t('Git Clone Repository'),
  ));
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function git_clone_form_system_file_system_settings_alter(&$form, &$form_state, $form_id) {
  // Move the core paths up.
  $form['file_public_path']['#weight'] = -10;
  $form['file_private_path']['#weight'] = -9;

  // Inject file_git_clone_path.
  $form['file_git_clone_path'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Git clone file system path'),
    '#default_value' => GitCloneStreamWrapper::directoryPath(),
    '#maxlength'     => 255,
    '#weight'        => -8,
    '#description'   => t('An existing local file system path for storing cloned git repositories. It should be writable by Drupal and not accessible over the web. See the online handbook for <a href="@handbook">more information about securing private files</a>.', array('@handbook' => 'http://drupal.org/documentation/modules/file')),
    '#after_build'   => array('system_check_directory'),
  );

  // Inject git_binary.
  $form['git_binary'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Git binary path'),
    '#default_value' => variable_get('git_binary', ''),
    '#maxlength'     => 255,
    '#weight'        => -7,
    '#description'   => t('The absolute path to the git binary on this server. (e.g. /usr/bin/git)'),
  );
}
